require 'rspec'
require './questionnaire'

RSpec.describe "Survey" do
  describe "#calculate_rating" do
    it "should calculate rating for last run" do
      answers = ["yes", "yes", "no", "no", "no"]
      rating_for_last_run = calculate_rating_for_last_run(answers)
      expect(rating_for_last_run).to eq(40)
    end

    it "should calculate average rating for all run" do
      STORE_NAME = "survey_test.pstore"
      @store = PStore.new(STORE_NAME)
      @store.transaction do
        @store[:answers] = [["yes", "yes", "yes", "yes", "yes"], ["yes", "no", "yes", "no", "no"]]
      end
      rating_for_all_run = calculate_average_rating_for_all_run
      expect(rating_for_all_run).to eq(70)
    end
  end
end
