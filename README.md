# Tendable Coding Assessment

## Usage

```sh
bundle
ruby questionnaire.rb
```

## Goal

The goal is to implement a survey where a user should be able to answer a series of Yes/No questions. After each run, a rating is calculated to let them know how they did. Another rating is also calculated to provide an overall score for all runs.

## Requirements

Possible question answers are: "Yes", "No", "Y", or "N" case insensitively to answer each question prompt.

The answers will need to be **persisted** so they can be used in calculations for subsequent runs >> it is proposed you use the pstore for this, already included in the Gemfile

After _each_ run the program should calculate and print a rating. The calculation for the rating is: `100 * number of yes answers / number of questions`.

The program should also print an average rating for all runs.

The questions can be found in questionnaire.rb

Ensure we can run your exercise

## Bonus Points

Updated readme with an explanation of your approach

Unit Tests

Code Comments

Dockerfile / Bash script if needed for us to run the exercise


Development README notes:
Development:

Handled Possible question answers: "Yes", "No", "Y", or "N" for case insensitive
Answers are persisted using PStore for subsequent runs
Rating calculation: 100 * number of yes answers / number of questions 
Average rating calculation for all runs
run program :

```bundle ruby questionnaire.rb```

Tests:

Known Issues

The chomp method may cause issues when running the RSpec tests. To resolve this, comment out the chomp method in the do_prompt method.

The implementation is tested using RSpec. The tests cover the following scenarios:

Calculating rating for the current run
Calculating average rating for all runs

Running the RSpec Tests

``` bundle exec rspec specs/questionnaire_spec.rb ```
