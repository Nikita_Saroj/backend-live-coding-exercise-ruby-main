require "pstore" # https://github.com/ruby/pstore

STORE_NAME = "tendable.pstore"
@store = PStore.new(STORE_NAME)

QUESTIONS = {
  "q1" => "Can you code in Ruby?",
  "q2" => "Can you code in JavaScript?",
  "q3" => "Can you code in Swift?",
  "q4" => "Can you code in Java?",
  "q5" => "Can you code in C#?"
}.freeze

# TODO: FULLY IMPLEMENT
def do_prompt
  # Ask each question and get an answer from the user's input.
  answers = []
  possible_ans = ["yes", "no", "y", "n"]
  QUESTIONS.each_key do |question_key|
    print QUESTIONS[question_key]
    ans = gets.chomp.downcase
    while !possible_ans.include?(ans)
      print "Invalid answer. Please enter Yes or No: "
      ans = gets.chomp.downcase
    end
    answers << ans
  end
  @store.transaction do
    @store[:answers] ||= []
    @store[:answers] << answers
  end
  puts "Rating for last run: #{calculate_rating_for_last_run(answers)}"
end

# caculates rating for last run based on answers
def calculate_rating_for_last_run(answers)
  total_yes_answers = answers.select{|ans| ans == "yes" || ans == "y"}.count
  ( 100 * total_yes_answers ) / QUESTIONS.count
end

# caculates avarage rating for all runs based on @store data 
def calculate_average_rating_for_all_run
  average_rating = 0
  @store.transaction do
    puts @store.inspect
    answers = @store[:answers]
    total_yes_answers = answers.flatten.select{|ans| ans == "yes" || ans == "y"}.count
    total_questions = answers.flatten.count
    average_rating = (100 * total_yes_answers) / total_questions
  end
  average_rating
end

def do_report
  puts "Average rating for all runs: #{calculate_average_rating_for_all_run}"
end

do_prompt
do_report